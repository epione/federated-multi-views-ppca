import numpy as np
from csv import writer
import pandas as pd
from sklearn import preprocessing
from scipy.stats import matrix_normal, multivariate_normal
import os

def csv_for_records(script_dir, scenario, DP):
    """
    This function creates csv for records
    :param script_dir: current directory
    :param scenario: str
    :param DP: bool
    :return csv files
    """
    
    # csv for saving stats for model performance
    results_csv_name = 'Model_comparison_' + scenario + '.csv'
    results_csv = os.path.join(script_dir, results_csv_name)
    if not os.path.isfile(results_csv):
        header = ['Test', 'Data', 'DP', 'clip', 'N_rounds', 'N_MAP_iter', 'N_EM_iter', 'D_k', 'N_centers', 'N_subj', 'N_comp',
                  'num_simu', 'WAIC', 'muk_tildemuk', 'Wk_tildeWk',
                  'Sigma2k_tildeSigma2k', 'Eigenval_cov', 'Max_eigen', 'MAE', 'MAE_test',
                  'accuracy_LDA_Test', 'conf_LDA_Test']

        with open(results_csv, "w", newline='') as f:
            writer(f, delimiter=',').writerow(header)  # write the header

    # csv file for saving local and global parameters
    Params_csv_name = 'Estimated_parameters_' + scenario + '.csv'
    Params_csv = os.path.join(script_dir, Params_csv_name)
    if not os.path.isfile(Params_csv):
        header = ['Test', 'Data', 'DP', 'clip', 'S_muk', 'S_Wk', 'alphak', 'betak']

        with open(Params_csv, "w", newline='') as f:
            writer(f, delimiter=',').writerow(header)  # write the header

    # csv file for saving evolution of standard deviations
    std_csv_name = 'Global_params_std_' + scenario + '.csv'
    std_csv = os.path.join(script_dir, std_csv_name)
    if not os.path.isfile(std_csv):
        header = ['Test', 'Data', 'N_centers', 'Round', 'Views', 'N_MAP_iter', 'N_EM_iter',
                  'DP', 'clip', 'sigma_til_muk', 'sigma_til_Wk', 'sigma_til_sigma2k']

        with open(std_csv, "w", newline='') as f:
            writer(f, delimiter=',').writerow(header)  # write the header

    if DP:
        # csv file for saving local sensitivity over all parameters
        script_dir = os.path.dirname(__file__)
        LS_csv_name = 'Local_Sensitivity_' + scenario + '.csv'
        LS_csv = os.path.join(script_dir, LS_csv_name)
        if not os.path.isfile(LS_csv):
            header = ['Test', 'Data', 'N_centers', 'Round', 'Views', 'N_MAP_iter', 'N_EM_iter', 
                    # 'LS_mu', 'LS_W', 'LS_sigma', 
                    # 'diff_perturbed_Wk1', 'diff_perturbed_Wk2', 'diff_perturbed_Wk3', 
                    'clip', 'epsilon', 'delta', 'LL', 'LL_1'
                    # , 'LL_2', 'LL_diff'
                    # , 'num_cl_W', 'num_cl_mu', 'num_cl_S'
                    ]

            with open(LS_csv, "w", newline='') as f:
                writer(f, delimiter=',').writerow(header)  # write the header

        return results_csv, Params_csv, std_csv, LS_csv
    else:
        return results_csv, Params_csv, std_csv

def append_list_as_row(file_name, list_of_elem):
    """
    This function is used to append results to csv
    :param file_name: str
    :param list_of_elem: list
    """
    # Open file in append mode
    with open(file_name, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        csv_writer = writer(write_obj)
        # Add contents of list as last row in the csv file
        csv_writer.writerow(list_of_elem)

def eigen_dec_emp_cov(X_simu_k, D_i, k):
    """
    This function performs eigen-decomposition of empirical covariance matrix
    :param X_simu_k: list of np.arrays
    :param D_i: list of integers
    :param k: integer
    :return list
    """

    X_simu_k_mean = sum(X_simu_k)/len(X_simu_k)
    Emp_cov = np.zeros((D_i[k],D_i[k]))
    for l in range(len(X_simu_k)):
        Emp_cov += (X_simu_k[l]-X_simu_k_mean).dot((X_simu_k[l]-X_simu_k_mean).T)
    Emp_cov = Emp_cov/(len(X_simu_k)-1)

    # eigenvalues decomposition and sort
    eigen_values, eigen_vectors = np.linalg.eig(Emp_cov)
    pairs = [(np.abs(eigen_values[i]), eigen_vectors[:, i], np.sign(eigen_values[i])) for i in
             range(len(eigen_values))]
    pairs = sorted(pairs, key=lambda x: x[0], reverse=True)

    eigen_val_sorted = []
    for pair in pairs:
        eigen_val_sorted.append(pair[0] * pair[2])

    eigen_value_sums = sum(eigen_values)
    # print('Explained Variance')
    # for i, pair in enumerate(pairs):
    #     print('Eigenvector {}: {}'.format(i, (pair[0] / eigen_value_sums).real))

    return eigen_val_sorted

# Save first columns of t_n_k to csv for plots
def t_n_k(XC, ViewsXC, D_i, CENTERS, K, norm):
    Tk = pd.DataFrame()  # t_n(k) for each center
    for c in CENTERS:
        T_n_c = XC[c].sort_values(XC[c].columns[0])
        ind = 0
        for k in range(K):
            if ViewsXC[c][k] == 1:
                XCK = T_n_c.iloc[:, ind:ind + 2]
                ind += D_i[k]
                XCK.columns = ['tnk_0', 'tnk_1']
                if norm == True:
                    x = XCK.values  #
                    min_max_scaler = preprocessing.MinMaxScaler()  #
                    x_scaled = min_max_scaler.fit_transform(x)  #
                    XCK = pd.DataFrame(x_scaled, index=XCK.index, columns=XCK.columns)
                XCK['Center'] = c + 1
                XCK['View'] = k + 1
                Tk = Tk.append(XCK, ignore_index=True)
    return Tk


# Evaluate difference among estimated local and global parameters
def Diffs(G_params,L_params_new,CENTERS,ViewsXC,K):
    Diff_1 = []
    Diff_2 = []
    Diff_3 = []
    tilde_muk = G_params[0]
    tilde_Wk = G_params[1]
    tilde_Sigma2k = G_params[2]
    for c in CENTERS:
        muk = L_params_new[0][c]
        Wk = L_params_new[1][c]
        Sigma2 = L_params_new[2][c]
        muk_tilde_muk = []
        Wk_tilde_Wk = []
        Sigma2k_tildeSigma2k = []
        for k in range(K):
            if ViewsXC[c][k] == 1:
                muk_tilde_muk.append(np.linalg.norm(muk[k] - tilde_muk[k]))
                Wk_tilde_Wk.append(np.linalg.norm(Wk[k] - tilde_Wk[k]))
                Sigma2k_tildeSigma2k.append(abs(Sigma2[k] - tilde_Sigma2k[k]))
        Diff_1.append(muk_tilde_muk)
        Diff_2.append(Wk_tilde_Wk)
        Diff_3.append(Sigma2k_tildeSigma2k)
    return Diff_1,Diff_2,Diff_3

def df_sim_tnk(global_model,K,D_i):
    # simulation of t_n(k)
    Tksim = pd.DataFrame() 
    Eigenval_cov = []
    Max_eigen = []
    X_simu = []
    for k in range(K):
        X_simu_k = []
        for s in range(1000):
            X_simu_k.append(global_model.simuTnk(k))
        X_simu.append(X_simu_k)
        # Estimation empirical covariance
        Eigenval_cov.append(eigen_dec_emp_cov(X_simu_k, D_i, k))
        Max_eigen.append(max(Eigenval_cov[k]))
    for k in range(K):
        Xs = X_simu[k][0]
        for s in range(1, len(X_simu[k])):
            Xs = np.concatenate((Xs, X_simu[k][s]), axis=1)
        Tn_k = pd.DataFrame({'T_n_0': list(Xs[0, :]), 'T_n_1': list(Xs[1, :]),
                                'View': [(k + 1) for _ in range(len(Xs[0, :]))]})
        Tksim = Tksim.append(Tn_k, ignore_index=True)
    return Tksim, Eigenval_cov, Max_eigen

def Initial_priors(K,q,D_i):
    q_i = [] 
    for i in D_i:
        if i <= q:
            q_i.append(i - 1)
        else:
            q_i.append(q)

    Wk = [np.zeros((D_i[k], q)).reshape(D_i[k],q) for k in range(K)]
    muk = [np.zeros(D_i[k]).reshape(D_i[k]) for k in range(K)]
    Sigma2 = [0.1 for _ in range(K)]
    Alpha = [2.05 for _ in range(K)]
    Beta = [0.105 for _ in range(K)]
    sigma_Wk = [1 for _ in range(K)]
    sigma_muk = [1 for _ in range(K)]
    sigma_sigma2k = [0.2 for _ in range(K)]

    return [muk,Wk,Sigma2,Alpha,Beta,sigma_muk,sigma_Wk,sigma_sigma2k]
