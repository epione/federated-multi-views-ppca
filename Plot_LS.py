import matplotlib.pyplot as plt
plt.style.use('ggplot')
import numpy as np
import pandas as pd
from math import sqrt
import os
import ast

script_dir = os.path.dirname(__file__)

cwd = os.getcwd()

scenario = 'GK'
Centers = 3

LS_res = pd.read_csv(cwd + '/Local_Sensitivity_' + scenario +'.csv', index_col=0)
std_gl = pd.read_csv(cwd + '/Global_params_std_' + scenario +'.csv', index_col=0)
LS_res = LS_res.loc[LS_res.N_centers==Centers]
std_gl = std_gl.loc[std_gl.N_centers==Centers]

Round = list(std_gl.Round.unique())
Round_ls = list(LS_res.Round.unique())
Views = ast.literal_eval(LS_res['Views'].values[0])
Epsilon_mean = []
Epsilon_std = []
Epsilon_min = []
Epsilon_max = []
Epsilon_median = []
Delta_mean = []
Delta_std = []
Delta_min = []
Delta_max = []
Delta_median = []
LL_median = []
LL_1_median = []
sigma_til_muk_median = [[] for _ in range(len(Views[0]))]
sigma_til_Wk_median = [[] for _ in range(len(Views[0]))]
sigma_til_sigma2k_median = [[] for _ in range(len(Views[0]))]

for r in Round:
    if r in Round_ls:
        Epsilon_round_r = []
        Delta_round_r = []
        LL_round_r = []
        LL_1_round_r = []
    sigma_til_muk_round_r = [[] for _ in range(len(Views[0]))]
    sigma_til_Wk_round_r = [[] for _ in range(len(Views[0]))]
    sigma_til_sigma2k_round_r = [[] for _ in range(len(Views[0]))]
    for index, row in LS_res.loc[LS_res['Round'] == r].iterrows():
        views_i = ast.literal_eval(row['Views'])
        if r in Round_ls:
            Epsilon_round_r.append(ast.literal_eval(row['epsilon'])[0])
            Delta_round_r.append(ast.literal_eval(row['delta'])[0])
            LL_round_r.append(ast.literal_eval(row['LL'])[0])
            LL_1_round_r.append(ast.literal_eval(row['LL_1'])[0])
    for index, row in std_gl.loc[std_gl['Round'] == r].iterrows():
        for k in range(len(Views[0])):
            sigma_til_muk_round_r[k].append(sqrt(ast.literal_eval(row['sigma_til_muk'])[k]))
            sigma_til_Wk_round_r[k].append(sqrt(ast.literal_eval(row['sigma_til_Wk'])[k]))
            sigma_til_sigma2k_round_r[k].append(sqrt(ast.literal_eval(row['sigma_til_sigma2k'])[k]))
    if r in Round_ls:
        Epsilon_mean.append(np.mean(np.array(Epsilon_round_r)))
        Epsilon_std.append(np.std(np.array(Epsilon_round_r)))
        Epsilon_min.append(np.min(np.array(Epsilon_round_r)))
        Epsilon_max.append(np.max(np.array(Epsilon_round_r)))
        Epsilon_median.append(np.median(np.array(Epsilon_round_r)))
        Delta_mean.append(np.mean(np.array(Delta_round_r)))
        Delta_std.append(np.std(np.array(Delta_round_r)))
        Delta_min.append(np.min(np.array(Delta_round_r)))
        Delta_max.append(np.max(np.array(Delta_round_r)))
        Delta_median.append(np.median(np.array(Delta_round_r)))
        LL_median.append(np.median(np.array(LL_round_r)))
        LL_1_median.append(np.median(np.array(LL_1_round_r)))
    for k in range(len(Views[0])):
        sigma_til_muk_median[k].append(np.median(np.array(sigma_til_muk_round_r[k])))
        sigma_til_Wk_median[k].append(np.median(np.array(sigma_til_Wk_round_r[k])))
        sigma_til_sigma2k_median[k].append(np.median(np.array(sigma_til_sigma2k_round_r[k])))

colors = ['b', 'r', 'g', 'y', 'k']

labels = ['CLINIC', 'MRI', 'FDG', 'AV45']

fig1 = plt.figure()

plt.xlabel('Round ($r$)')
plt.ylabel('$\delta$')
plt.plot(Round_ls, Delta_mean)
plt.fill_between(Round_ls, list(np.subtract(Delta_mean,Delta_std)), list(np.add(Delta_mean,Delta_std)), alpha = 0.1)

fig2 = plt.figure()

plt.xlabel('Round ($r$)')
plt.ylabel('$\\varepsilon$')
plt.plot(Round_ls, Epsilon_mean)
plt.fill_between(Round_ls, list(np.subtract(Epsilon_mean,Epsilon_std)), list(np.add(Epsilon_mean,Epsilon_std)), alpha = 0.1)

fig3 = plt.figure()

plt.xlabel('Round ($r$)')
plt.ylabel('LL with perturbed W')
plt.plot(Round_ls, LL_median, color = colors[0], label = 'LL')
plt.plot(Round_ls, LL_1_median, color = colors[1], label = 'LL, perturbation')
plt.legend()

fig4 = plt.figure(figsize=(5*3, 10), dpi=150)
names = [['$W^{(CLINIC)}$','$W^{(MRI)}$','$W^{(FDG)}$','$W^{(AV45)}$'],
         ['$\mu^{(CLINIC)}$','$\mu^{(MRI)}$','$\mu^{(FDG)}$','$\mu^{(AV45)}$'],
         ['${\sigma^{(CLINIC)}}^2$','${\sigma^{(MRI)}}^2$','${\sigma^{(FDG)}}^2$','${\sigma^{(AV45)}}^2$']]

plt.subplot(131)
for k in range(len(Views[0])):
    plt.plot(Round, sigma_til_muk_median[k], color = colors[k], label = names[1][k])
    plt.legend(loc='lower right')

plt.subplot(132)
for k in range(len(Views[0])):
    plt.plot(Round, sigma_til_Wk_median[k], color = colors[k], label = names[0][k])
    plt.legend(loc='lower right')

plt.subplot(133)
for k in range(len(Views[0])):
    plt.plot(Round, sigma_til_sigma2k_median[k], color = colors[k], label = names[2][k])
    plt.legend(loc='lower right')

plt.show()