import numpy as np
import pandas as pd
from math import log
from scipy.special import digamma
from scipy.stats import matrix_normal, multivariate_normal, invgamma, norm

from .func_master import inv_digamma

# MASTER

class Fed_MVPPCA_MASTER:
    """
    This class represents the central optimization
    """
    def __init__(self, 
                round: int=None, 
                n_centers: int=None, 
                local_params: list=None, 
                dim_views: list=None,
                n_components: int=None, 
                rho: float=None):

        """ Constructor of the class

        Args:
            round (int): current comunication round. Defaults to None
            n_centers (int): number of training centers. Defaults to None
            local_params (list): contains the optimized local parameters at the previous round. Defaults to None
            dim_views (list): list containing the dimension of each view, len(dim_views)=tot num views.
                              Defaults to None
            n_components (int): the latent dimension. Defaults to None
            rho (float): used exclusively if only one center is contributing to the estimation of one view/group parameters, 
                         in order to have a non-zero variance for the global parameter. Defaults to None
        """
        self.dim_views = dim_views 
        self.n_views = len(self.dim_views)
        self.n_components = n_components 
        self.n_centers = n_centers

        self.rho = rho

        self.round = round

        self.local_params = local_params
        self.mukC = self.local_params[0]
        self.WkC = self.local_params[1]
        self.Sigma2kC = self.local_params[2]

        # Global parameters, to be updated
        self.tilde_muk = None
        self.tilde_Wk = None
        self.tilde_Sigma2k = None
        self.Alpha = None
        self.Beta = None
        self.sigma_til_muk = None
        self.sigma_til_Wk = None
        self.sigma_til_sigma2k = None

        # participating clients for current round and for parameter
        self.Tot_C_k_W = None
        self.Tot_C_k_mu = None
        self.Tot_C_k_S = None

    def fit(self):
        """
        This function perform the ML optimization at the master level for the current round. 
        It returns the dataframe with information of convergence for each global parameter.
        :return pandas dataframe
        """
        K = self.n_views
        corr_det_inv = 1e-35

        # evaluate total number of centers having measurements for each view
        self.count_participating_clients()

        # Create list of historic results as dataframe
        results_df = pd.DataFrame()

        # ================================== #
        #           ML OPTIMIZATION          #
        # ================================== #
        tilde_muk, tilde_Wk, tilde_Sigma2k, sigma_til_muk, sigma_til_Wk = self.eval_gauss_global_params(corr_det_inv)
        Alpha, Beta, sigma_til_sigma2k = self.eval_inv_gamma(tilde_Sigma2k,corr_det_inv)

        # Update global parameters
        self.tilde_muk = tilde_muk
        self.tilde_Wk = tilde_Wk
        self.tilde_Sigma2k = tilde_Sigma2k
        self.Alpha = Alpha
        self.Beta = Beta
        self.sigma_til_muk = sigma_til_muk
        self.sigma_til_Wk = sigma_til_Wk
        self.sigma_til_sigma2k = sigma_til_sigma2k

        # Append results
        results_iter = pd.Series(name=self.round, dtype=float)
        results_iter['round'] = self.round
        for k in range(K):
            name1 = 'norm_tilde_mu_' + str(k + 1)
            name2 = 'norm_tilde_W_' + str(k + 1)
            name3 = 'tilde_sigma2_' + str(k + 1)
            name4 = 'sigma2_tilde_mu_' + str(k + 1)
            name5 = 'sigma2_tilde_W_' + str(k + 1)
            name6 = 'sigma2_tilde_sigma2_' + str(k + 1)
            if self.Tot_C_k_mu[k] >= 1:
                results_iter[name1] = np.linalg.norm(tilde_muk[k])
                results_iter[name4] = sigma_til_muk[k]
            else:
                results_iter[name1] = 0
                results_iter[name4] = 0
            if self.Tot_C_k_W[k] >= 1:
                results_iter[name2] = np.linalg.norm(tilde_Wk[k])
                results_iter[name5] = sigma_til_Wk[k]
            else:
                results_iter[name2] = 0
                results_iter[name5] = 0
            if self.Tot_C_k_S[k] >= 1:
                results_iter[name3] = tilde_Sigma2k[k]
                results_iter[name6] = sigma_til_sigma2k[k]
            else:
                results_iter[name3] = 0
                results_iter[name6] = 0

        results_df = results_df.append(results_iter)

        return results_df

    @property
    def global_params(self):
        """
        :return lists: global parameters to be communicated to the centers
        """
        # Return global parameters
        return self.tilde_muk, self.tilde_Wk, self.tilde_Sigma2k, self.Alpha, self.Beta, \
            self.sigma_til_muk, self.sigma_til_Wk, self.sigma_til_sigma2k

    @property
    def effective_num_clients(self):
        """
        :return lists (the effective number of participating clients per parameter for the current round)
        """
        return self.Tot_C_k_W, self.Tot_C_k_mu, self.Tot_C_k_S

    def eval_gauss_global_params(self,corr_det_inv):
        """
        This function performs ML estimation for normally distributed parameters
        :return lists of np.arrays (tilde_muk, tilde_Wk)
        :return lists of floats (tilde_Sigma2k, sigma_til_muk, sigma_til_Wk)
        """
        # ML estimator for gaussian parameters and mean of sigma2
        q = self.n_components
        D_i = self.dim_views
        K = self.n_views
        C = self.n_centers

        tilde_muk = []
        tilde_Wk = []
        tilde_Sigma2k = []
        sigma_til_muk = []
        sigma_til_Wk = []
        for k in range(K):
            tilmuk = np.zeros((D_i[k], 1))
            tilWk = np.zeros((D_i[k], q))
            tilSk = 0.0
            for c in range(C):
                if type(self.mukC[c][k]) is not str:
                    tilmuk+=self.mukC[c][k]
                if type(self.WkC[c][k]) is not str:
                    tilWk += self.WkC[c][k]
                if type(self.Sigma2kC[c][k]) is not str:
                    tilSk += self.Sigma2kC[c][k]
            
            if self.Tot_C_k_S[k] >= 1:
                tilde_Sigma2k.append(tilSk / self.Tot_C_k_S[k])
            else:
                tilde_Sigma2k.append(None)

            if self.Tot_C_k_W[k] > 1:
                tilde_Wk.append(1.0 / self.Tot_C_k_W[k] * tilWk)
                sigWk = 0.0
                for c in range(C):
                    if type(self.WkC[c][k]) is not str:
                        sigWk += np.matrix.trace((self.WkC[c][k] - tilde_Wk[k]).T.dot(self.WkC[c][k] - tilde_Wk[k]))
                if sigWk == 0.0:
                    sigma_til_Wk.append(corr_det_inv)
                else:
                    sigma_til_Wk_temp = sigWk / (self.Tot_C_k_W[k] * D_i[k] * q)
                    sigma_til_Wk.append(sigma_til_Wk_temp*min(1,np.linalg.norm(tilde_Wk[k])/(5*sigma_til_Wk_temp)))
            elif self.Tot_C_k_W[k] == 1:
                tilde_Wk.append(1.0 / self.Tot_C_k_W[k] * tilWk)
                sigma_til_Wk.append(self.rho)
            else:
                tilde_Wk.append(None)
                sigma_til_Wk.append(None)

            if self.Tot_C_k_mu[k] > 1:
                tilde_muk.append(1.0/self.Tot_C_k_mu[k]*tilmuk)
                sigmuk = 0.0
                for c in range(C):
                    if type(self.mukC[c][k]) is not str:
                        sigmuk+=float((self.mukC[c][k]-tilde_muk[k]).T.dot(self.mukC[c][k]-tilde_muk[k]))
                if sigmuk == 0.0:
                    sigma_til_muk.append(corr_det_inv)
                else:
                    sigma_til_muk.append(sigmuk/(self.Tot_C_k_mu[k]*D_i[k]))
            elif self.Tot_C_k_mu[k] == 1:
                tilde_muk.append(1.0/self.Tot_C_k_mu[k]*tilmuk)
                sigma_til_muk.append(self.rho)
            else:
                tilde_muk.append(None)
                sigma_til_muk.append(None)

        return tilde_muk, tilde_Wk, tilde_Sigma2k, sigma_til_muk, sigma_til_Wk

    def eval_inv_gamma(self,tilde_Sigma2k,corr_det_inv):
        """
        This function performs ML estimation for inverse gamma parameters
        :return lists of floats 
        """
        # Alpha, Beta ML method
        K = self.n_views
        C = self.n_centers

        Alpha = []
        Beta = []
        sigma_til_sigma2k = []
        for k in range(K):
            if self.Tot_C_k_S[k] >= 1:
                Ck_1 = 0.0
                Ck_2 = 0.0
                varSk = 0.0
                for c in range(C):
                    if type(self.Sigma2kC[c][k]) is not str:
                        Ck_1 += 1.0 / self.Sigma2kC[c][k]
                        Ck_2 += log(self.Sigma2kC[c][k])
                        varSk += (self.Sigma2kC[c][k] - tilde_Sigma2k[k]) ** 2
                Ck = -log(Ck_1) - Ck_2 / self.Tot_C_k_S[k]
                if varSk == 0.0:
                    varSk = 1e-16
                if self.Tot_C_k_S[k] == 1:
                    alphak = (tilde_Sigma2k[k] ** 2) / (varSk) + 2
                else:
                    alphak = (tilde_Sigma2k[k] ** 2) / (varSk / (self.Tot_C_k_S[k] - 1.0)) + 2
                for cov in range(10):
                    alphak = inv_digamma(y=log(self.Tot_C_k_S[k]*alphak)+Ck)
                if alphak<=2:
                    alphak = 2+1e-5
                betak = (self.Tot_C_k_S[k] * alphak) / Ck_1
                var_sigmak = betak ** 2 / (((alphak - 1) ** 2) * (alphak - 2))
                Beta.append(betak)
                Alpha.append(alphak)
                sigma_til_sigma2k.append(var_sigmak)
            else:
                Beta.append(None)
                Alpha.append(None)

        return Alpha, Beta, sigma_til_sigma2k

    def simuTnk(self, view):
        """
        This function allows to sample t_n(k) from the posterior
        :return np.array
        """
        # Sample t_n(k) from posterior
        q = self.n_components
        D_i = self.dim_views

        Wk = matrix_normal.rvs(mean=self.tilde_Wk[view].reshape(D_i[view], q),
                               rowcov=np.eye(D_i[view]),
                               colcov=self.sigma_til_Wk[view] * np.eye(q)).reshape(D_i[view], q)
        muk = multivariate_normal.rvs(mean=self.tilde_muk[view].reshape(D_i[view]),
                                      cov=self.sigma_til_muk[view] * np.eye(D_i[view])).reshape(D_i[view], 1)
        sigma2k = float(invgamma.rvs(a=self.Alpha[view], scale=self.Beta[view]))

        mean = muk
        cov = Wk.dot(Wk.T) + sigma2k * np.eye(D_i[view])

        Tngk = multivariate_normal.rvs(mean=mean.reshape(D_i[view]), cov=cov).reshape(D_i[view], 1)

        return Tngk

    def count_participating_clients(self):
        """
        This function evaluates the effective number of participating clients per parameter for the current round
        :return lists of integers
        """
        K = self.n_views
        C = self.n_centers

        Tot_C_k_W = []
        Tot_C_k_mu = []
        Tot_C_k_S = []

        for k in range(K):
            TotCkW = 0
            TotCkmu = 0
            TotCkS = 0
            for c in range(C):
                if type(self.WkC[c][k]) is not str:
                    TotCkW += 1
                if type(self.mukC[c][k]) is not str:
                    TotCkmu += 1
                if type(self.Sigma2kC[c][k]) is not str:
                    TotCkS += 1
            Tot_C_k_W.append(TotCkW)
            Tot_C_k_mu.append(TotCkmu)
            Tot_C_k_S.append(TotCkS)

        self.Tot_C_k_W = Tot_C_k_W
        self.Tot_C_k_mu = Tot_C_k_mu
        self.Tot_C_k_S = Tot_C_k_S
