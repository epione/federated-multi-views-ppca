import numpy as np
from numpy.linalg import solve, LinAlgError
import pandas as pd
from math import log, sqrt, exp 
from sklearn.metrics import mean_absolute_error
from scipy.linalg import block_diag
from scipy.special import logsumexp
from scipy.stats import matrix_normal, multivariate_normal, invgamma, laplace, norm
from copy import deepcopy

from .func_client import normalize_data, view_data, compute_Cck, compute_inv_term1_muck, random_init_Sk, random_init_Wk

# CENTER

class Fed_MVPPCA_CLIENT:
    """
    This class represents the local optimization in each training center
    """
    def __init__(self,  
                 X,
                 id_client: str=None,
                 round: int=None, 
                 ep_count: int=None,
                 norm: bool=True, 
                 ViewsX: list=None, 
                 dim_views: list=None, 
                 n_components: int=None, 
                 global_params: list=None, 
                 n_iterations: int=None, 
                 DP: dict=None):

        """ Constructor of the class

        Args:
            X (pandas df): pandas local dataframe for client id_client
            id_client (str): the id of the training client. Defaults to None
            round (int): current comunication round. Defaults to None
            ep_count (int): save the total number of local iterations performed by client id_client.
                            Defaults to None
            norm (bool): boolean to decide if the dataset should be normalized or not. Defaults to True.
            ViewsX (list): list containing 1 at position i if the center id_client disposes of data for the i-th view,
                    0 otherwise. len(ViewsX)=tot num views. Defaults to None
            dim_views (list): list containing the dimension of each view, len(dim_views)=tot num views.
                              Defaults to None
            n_components (int): the latent dimension. Defaults to None
            global_params (list): list containing the optimized gobal parameters at the last iteration.
                                  Defaults to None
            n_iterations (int): the number of EM/MAP iterations for the current round. Defaults to None
            DP (dict): dictionary for differential privacy. Defaults to None
        """

        self.dim_views = dim_views
        self.n_views = len(self.dim_views)
        self.ViewsX = ViewsX
        self.index = self.ViewsX.index(1)  # first view measured in current center
        self.n_components = n_components
        self.n_iterations = n_iterations
        self.id_client = id_client
        self.round = round
        self.ep_count = ep_count

        # normalize the local dataset if norm is True
        self.dataset = normalize_data(X) if norm else X

        print(f'==Center {self.id_client}==')

        # verifications:
        assert self.n_views == len(self.ViewsX), 'List of indicator functions for observed views must have the same lengh '\
                                                 'as the number of views'

        D_i_C = 0
        for k in range(self.n_views):
            if self.ViewsX[k] == 1:
                D_i_C += self.dim_views[k]

        assert self.dataset.shape[1] == D_i_C, 'Dataset size must be equal to sum of views dimensions for current center'

        # self.Xk is a list contianing the view-specific local datasets
        self.Xk = view_data(self.round,self.n_views,self.dim_views,self.ViewsX,self.dataset)
        if round == 1:
            for k in range(self.n_views):
                if self.ViewsX[k] == 1:
                    if round == 1:
                        print(self.Xk[k].head())
                        print(f'Shape of X%i: {self.Xk[k].shape}' % k)
            print(f'Indicator function views = {self.ViewsX}')

        # The "effective" latent space dimension per view (at least equal to original view dimension-1):
        # if q>dim_views[k], than all columns of W_k with index >=q will be set to 0
        self.q_i = self.effectitve_latent_dim()

        if self.n_components > min(self.dim_views):
            print("Warning: number of components greater than view dimensions")
            print(self.q_i)

        # global parameters to be used as priors for local optimization, if communicated by the master
        self.global_params = global_params 
        if self.global_params is not None:
            self.tilde_muk = self.global_params[0]
            self.tilde_Wk = self.global_params[1]
            self.tilde_Sigma2k = self.global_params[2]
            self.Alpha = self.global_params[3]
            self.Beta = self.global_params[4]
            self.sigma_til_muk = self.global_params[5]
            self.sigma_til_Wk = self.global_params[6]
            self.sigma_til_sigma2k = self.global_params[7]
            for k in range(self.n_views):
                if self.ViewsX[k] == 1:
                    self.tilde_muk[k] = deepcopy(self.tilde_muk[k].reshape(self.dim_views[k], 1))
                    self.tilde_Wk[k] = deepcopy(self.tilde_Wk[k].reshape(self.dim_views[k], self.n_components))
        
        # DP dictionary can be passed and drive optimization with differential privacy
        # DP = {DP: True, DP_e: 1, DP_d: 1, DP_r: 10}
        if DP is not None:
            for i in DP:
                setattr(self, i, DP[i])
            if self.DP:
                self.e_d = None
                self.real_muk = None
                self.real_Wk = None
                self.real_Sigma2 = None

        # local parameters to be optimized
        self.muk = None
        self.Wk = None
        self.Sigma2 = None
        self.E_L_c = None

    def fit(self):
        """
        This function perform the local EM/MAP optimization in client client_id for the current round. 
        It returns the dataframe with information of convergence for each parameter, and the likelihood.
        :return pandas dataframe
        """

        N = self.dataset.shape[0]

        # initialize W and Sigma2
        Wk_0, Sigma2_0 = self.initial_loc_params()

        # =================================== #
        # Create dataset for historic results #
        # =================================== #
        # epochs indices for saving results
        sp_arr = np.arange(1, self.n_iterations + 1)[np.round(
            np.linspace(0, len(np.arange(1, self.n_iterations + 1)) - 1, int(self.n_iterations / 3))).astype(
            int)]
        # Create list of historic results as dataframe
        results_df = pd.DataFrame()

        # ================================== #
        #  ITERATION STARTS HERE             #
        # ================================== #
        Wk = deepcopy(Wk_0)
        Sigma2 = deepcopy(Sigma2_0)
        for i in range(1, self.n_iterations + 1):
            self.ep_count += 1
            muk, Wk, Sigma2, ELL = self.EM_Optimization(N,Wk,Sigma2,self.Xk)

            # ================================== #
            # Append results                     #
            # ================================== #
            if i in sp_arr:
                results_iter = self.append_results_local(ELL, Wk, muk, Sigma2)
                results_df = results_df.append(results_iter)

        # update local parameters
        self.muk = muk
        self.Wk = Wk
        self.Sigma2 = Sigma2
        self.E_L_c = [ELL]

        # If differential privacy is true, the sensitivity of each param is evaluated
        # and parameters are perturbed accordingly
        if self.DP:
            print('==Evaluating diff private params==')
            # before performing parameter perturbation, we save the "true" optimized parameters which could be used
            # for initialization at the next communication round in case no prior is provided for some specific parameter
            self.real_muk = deepcopy(self.muk)
            self.real_Wk = deepcopy(self.Wk)
            self.real_Sigma2 = deepcopy(self.Sigma2)
            self.DP_parameters()

        return results_df

    @property
    def local_params(self):
        """
        :return three lists: local parameters to be communicated to the master
        """
        return self.muk, self.Wk, self.Sigma2

    @property
    def real_loc_params(self):
        """
        :return three lists: local parameters before perturbation
        """
        return self.real_muk, self.real_Wk, self.real_Sigma2

    @property
    def tot_epochs(self):
        """
        :return integer: total number of epochs done
        """
        return self.ep_count

    @property
    def eps_del(self):
        """
        :return list: effective privacy costs
        """
        return self.e_d

    @property
    def Sens_local(self):
        """
        :return list: local sensitivity for each parameter
        """
        return self.sensitivity

    @property
    def LL(self):
        """
        :return list: expected total loglikelihood before and after parameter perturbation
        """
        return self.E_L_c

    def effectitve_latent_dim(self):
        """
        This function returns a list with the effective latent dimension to be considered for each view:
        if the dimension of view k d_k >= q, the user defined latent dimension, hence only the first d_k-1 
        column of W_k will be optimized, the remaining one will be set to 0.
        :return list
        """
        q_i = [] 
        for i in self.dim_views:
            if i <= self.n_components:
                q_i.append(i - 1)
            else:
                q_i.append(self.n_components)
        return q_i

    def WAIC_score(self, S):
        """
        This function returns the WAIC score for the current center
        :param S: integer (number of simulations for WAIC evaluation)
        :return float WAIC
        :return integer min(len_logP_N)
        """
        d = self.dataset.shape[1]
        N = self.dataset.shape[0]

        elppd2 = 0.0

        logP_N = [[] for _ in range(N)]
        for s in range(S):
            Cs, mus = self.Sample_C_musk()
            for n in range(N):
                try:
                    logP_N[n].append(
                        multivariate_normal.logpdf(self.dataset.iloc[n].values.reshape(d), mus.reshape(d), Cs))
                except LinAlgError:
                    pass

        len_logP_N = [len(logP_N[n]) for n in range(N)]

        for n in range(N):
            if len(logP_N[n])>0:
                LSE = logsumexp(logP_N[n])
                VarP = np.var(logP_N[n])
                elppd2 += LSE - VarP - log(len_logP_N[n])

        if min(len_logP_N) < S:
            print(f'Number of simulations for WAIC: {min(len_logP_N)}')

        WAIC = -2*elppd2

        return WAIC, min(len_logP_N)

    def Sample_C_musk(self):
        """
        This function returns the dxd covariance matrix C (WWT+Psi)Wk, 
        and dx1 vector mu (needed for WAIC score evaluation)
        :return np.array (d x d)
        :return np.array (d x 1)
        """
        K = self.n_views
        D_i = self.dim_views

        Wsk, musk, sigmaks = self.Sample_LP(self.index)
        Psi = sigmaks * np.eye(D_i[self.index])
        for k in range(self.index + 1, K):
            if self.ViewsX[k] == 1:
                Wsk_k, musk_k, sigmaks = self.Sample_LP(k)
                Wsk = np.concatenate((Wsk, Wsk_k), axis=0)
                musk = np.concatenate((musk, musk_k), axis=0)
                Psi = block_diag(Psi, sigmaks * np.eye(D_i[k]))
        C = Wsk.dot(Wsk.T) + Psi

        return C, musk

    def Sample_LP(self,k):
        """
        This function, for a given view k, returns Wk, muk and sigmak sampled from the global distribution
        :param k: integer corresponding to the view to sample
        :return np.array Wk (d_k x q)
        :return np.array muk (d_k x 1)
        :return float sigmak > 0
        """
        q = self.n_components
        D_i = self.dim_views

        Wk = matrix_normal.rvs(mean=self.tilde_Wk[k],
                                rowcov=np.eye(D_i[k]),
                                colcov=self.sigma_til_Wk[k] * np.eye(q)).reshape(D_i[k], q)
        muk = multivariate_normal.rvs(mean=self.tilde_muk[k].reshape(D_i[k]),
                                       cov=self.sigma_til_muk[k] * np.eye(D_i[k])).reshape(
            D_i[k], 1)
        sigmak = float(invgamma.rvs(a=self.Alpha[k], scale=self.Beta[k]))

        return Wk, muk, sigmak

    def simuXn(self):
        """
        This function generates x_n using p(x_n|t_n) and global parameters.
        :return np.array 
        """
        d = self.dataset.shape[1]
        N = self.dataset.shape[0]

        # if self.DP:
        #     Wk = deepcopy(self.real_Wk)
        #     muk = deepcopy(self.real_muk)
        #     Sigma2 = deepcopy(self.real_Sigma2)
        # else:
        #     Wk = deepcopy(self.Wk)
        #     muk = deepcopy(self.muk)
        #     Sigma2 = deepcopy(self.Sigma2)

        mu = self.concat_params(self.tilde_muk)
        M, B = self.eval_MB(self.tilde_Wk, self.tilde_Sigma2k)

        ran_n = np.random.randint(0, N - 1)

        Xng = np.random.multivariate_normal(mean=list((M.dot(B).dot(self.dataset.iloc[ran_n].values.reshape(d, 1) - mu)).T[0]), cov=M)

        return Xng

    def MAE(self):
        """
        This function evaluates the MAE using global parameters
        :return float
        """
        d = self.dataset.shape[1]
        q = self.n_components
        N = self.dataset.shape[0]

        mu = self.concat_params(self.tilde_muk)
        W = self.concat_params(self.tilde_Wk)
        M, B = self.eval_MB(self.tilde_Wk, self.tilde_Sigma2k)

        T_true = self.dataset.values.tolist()

        T_pred = []
        for n in range(N):
            Xng = (M.dot(B).dot(self.dataset.iloc[n].values.reshape(d, 1) - mu)).reshape(q, 1)
            T_pred.append((W.dot(Xng) + mu).reshape(d))

        MAE = mean_absolute_error(T_true, T_pred)

        return MAE

    def simu_latent(self):
        """
        This function allows sampling of x_n (latent variables) from the posterior distribution 
        (with global parameters).
        :return pandas dataframe
        """
        d = self.dataset.shape[1]
        q = self.n_components
        N = self.dataset.shape[0]

        mu = self.concat_params(self.tilde_muk)
        M, B = self.eval_MB(self.tilde_Wk, self.tilde_Sigma2k)

        Xn = [(M.dot(B).dot(self.dataset.iloc[n].values.reshape(d, 1) - mu)).reshape(1, q) for n in range(N)]

        df = pd.DataFrame(np.vstack(Xn), index=self.dataset.index)

        return df

    def concat_params(self, park):
        """
        This function concatenates parameters from a list
        :param park: list of vectors/matrices to concatenate
        :return np.array
        """
        K = self.n_views

        par = park[self.index]
        for k in range(self.index + 1, K):
            if self.ViewsX[k] == 1:
                par = np.concatenate((par, park[k]), axis=0)

        return par

    def pred_missing_view(self,view):
        """
        This function predicts missing views for each subject usign global parameters
        :param view: integer corresponding to the view to be predicted
        :return pandas dataframe
        """
        d = self.dataset.shape[1]
        D_i = self.dim_views
        q = self.n_components
        N = self.dataset.shape[0]

        mu = self.concat_params(self.tilde_muk)
        M, B = self.eval_MB(self.tilde_Wk, self.tilde_Sigma2k)

        T_pred = []
        for n in range(N):
            Xng = (M.dot(B).dot(self.dataset.iloc[n].values.reshape(d, 1) - mu)).reshape(q, 1)
            T_pred.append((self.tilde_Wk[view].dot(Xng) + self.tilde_muk[view]).reshape(D_i[view]))

        df = pd.DataFrame(np.row_stack(T_pred))

        return df

    def EM_Optimization(self,N,Wk,Sigma2,Xk):
        """
        This function manage one iteration of EM/MAP optimization
        :param N: integer (number of subjects in the dataset)
        :param Wk: list of matrices (d_k x q)
        :param Sigma2: list of floats > 0
        :param Xk: list of view-specific datasets
        :return list of np.arrays (d_k x 1) muk
        :return list of np.arraya (d_k x q) Wk
        :return list of floats > 0 Sigma2
        :return floaT E_L_c
        """
        K = self.n_views

        # mu
        muk = self.eval_muk(N, Wk, Sigma2, Xk)
        # matreces M, B
        M, B = self.eval_MB(Wk, Sigma2)
        # ||(tn^kg-mu^k)||2, (tn^kg-mu^k)
        norm2, tn_muk = self.compute_access_vectors(N,muk,Xk)

        # ================================== #
        # E-step                             #
        # ================================== #

        # evaluate mean, second moment for each x_n and expected complete log-likelihood
        E_X, E_X_2, E_L_c = self.compute_moments_LL(N, Sigma2, norm2, tn_muk, Wk, M, B)

        # ================================== #
        # M-step                             #
        # ================================== #

        #  W, Sigma2
        Wk, Sigma2_new = self.eval_Wk_Sigma2_new(N, norm2, tn_muk, E_X, E_X_2, Sigma2)
        # Check Sigma2_new>0
        for k in range(K):
            if self.ViewsX[k] == 1:
                if Sigma2_new[k] > 0:
                    Sigma2[k] = Sigma2_new[k]
                else:
                    print(f'Warning: sigma2(%i)<0 (={Sigma2_new[k]})' % (k + 1))

        return muk, Wk, Sigma2, E_L_c

    def DP_parameters(self):
        """
        This function is in charge of the following tasks:
        - Compute the difference of the current updated parameters and global parameters at the previous round
        - Clip the norm of the difference
        - Perturb the difference 
        - Evaluate LL after perturbation
        """
        # Evaluate differences and add noise
        e_d_c = [0.0,0.0]
        for k in range(len(self.ViewsX)):
            if self.ViewsX[k] == 1:
                e_d_c[0]+=(self.DP_e[0][k]+self.DP_e[1][k]+self.DP_e[2][k])
                e_d_c[1]+=(self.DP_d[0][k]+self.DP_d[1][k])
                # Wk
                self.update_Wk(k)
                # muk
                self.update_muk(k)
                # sigmak
                self.update_sigmak(k)
        ELc_pert = self.E_L_c_perturbed()
        self.E_L_c.append(ELc_pert)
        self.e_d = deepcopy(e_d_c)
        print(self.E_L_c)

    def update_Wk(self, k):
        """
        This function performs the perturbation of the difference between Wk
        and tilde_Wk at the previous round after clipping wrt to the standard deviation of tilde_Wk.
        :param k: integer (view index)
        """
        q = self.n_components
        d_k = self.dim_views[k]

        c_gauss = sqrt(log(2/(sqrt(16*self.DP_d[0][k]+1)-1)))
        c_gauss2 = ((c_gauss+sqrt(c_gauss**2+self.DP_e[0][k]))**2)/2

        if self.DP_c == 'DIFF':
            const = self.DP_c_const
            if self.global_params is not None:
                diff_wk = deepcopy(self.real_Wk[k]-self.tilde_Wk[k].reshape(d_k, q))
                C = sqrt(self.sigma_til_Wk[k])*const
                Clip = max(1,np.linalg.norm(diff_wk)/C)
                
                pert_wk = matrix_normal.rvs(mean = np.zeros((d_k, q)),rowcov=np.eye(d_k),\
                            colcov=(c_gauss2 * C**2 * 4 / (self.DP_e[0][k])**2) * np.eye(q)).reshape(d_k, q)

                if self.q_i[k] < q:
                    pert_wk[:, self.q_i[k]:q] = 0

                self.Wk[k] = deepcopy((diff_wk/Clip+pert_wk+self.tilde_Wk[k]).reshape(d_k, q))
            else:
                C = np.linalg.norm(self.real_Wk[k])/4.
                pert_wk = matrix_normal.rvs(mean = np.zeros((d_k, q)),rowcov=np.eye(d_k),\
                        colcov=((c_gauss2 * C**2 * 4 / (self.DP_e[0][k])**2) * np.eye(q))).reshape(d_k, q)
                self.Wk[k] = deepcopy(self.real_Wk[k]/max(1,np.linalg.norm(self.real_Wk[k])/C) + pert_wk)
        elif self.DP_c =='MODEL':
            C = np.linalg.norm(self.tilde_Wk[k])/4.
            pert_wk = matrix_normal.rvs(mean = np.zeros((d_k, q)),rowcov=np.eye(d_k),\
                    colcov=((c_gauss2 * C**2 * 4 / (self.DP_e[0][k])**2) * np.eye(q))).reshape(d_k, q)
            self.Wk[k] = deepcopy(self.real_Wk[k]/max(1,np.linalg.norm(self.real_Wk[k])/C) + pert_wk)

    def update_muk(self, k):
        """
        This function performs the perturbation of the difference between muk
        and tilde_muk at the previous round after clipping wrt to the standard deviation of tilde_muk.
        :param k: integer (view index)
        """
        d_k = self.dim_views[k]
        
        c_gauss = sqrt(log(2/(sqrt(16*self.DP_d[1][k]+1)-1)))
        c_gauss2 = ((c_gauss+sqrt(c_gauss**2+self.DP_e[1][k]))**2)/2

        if self.DP_c == 'DIFF':
            const = self.DP_c_const
            if self.global_params is not None:
                diff_muk = deepcopy(self.muk[k]-self.tilde_muk[k].reshape(d_k, 1))
                C = sqrt(self.sigma_til_muk[k])*const
                Clip = max(1,np.linalg.norm(diff_muk)/C)

                pert_muk = multivariate_normal.rvs(cov=(c_gauss2 * C**2 * 4 / (self.DP_e[1][k])**2) * np.eye(d_k)).reshape(d_k, 1)
                                
                self.muk[k] = deepcopy((diff_muk/Clip+pert_muk+self.tilde_muk[k]).reshape(d_k, 1))
            else:
                C = np.linalg.norm(self.real_muk[k])/4.
                pert_muk = multivariate_normal.rvs(cov=(c_gauss2 * C**2 * 4 / (self.DP_e[1][k])**2) * np.eye(d_k)).reshape(d_k, 1)
                self.muk[k] = deepcopy(self.real_muk[k]/(max(1,np.linalg.norm(self.real_muk[k])/C) + pert_muk))

        elif self.DP_c =='MODEL':
            C = np.linalg.norm(self.tilde_muk[k])/4.
            pert_muk = multivariate_normal.rvs(cov=(c_gauss2 * C**2 * 4 / (self.DP_e[1][k])**2) * np.eye(d_k)).reshape(d_k, 1)
            self.muk[k] = deepcopy(self.real_muk[k]/(max(1,np.linalg.norm(self.real_muk[k])/C) + pert_muk))

    def update_sigmak(self, k):
        """
        This function performs the perturbation of the difference between sigma2k
        and tilde_sigma2k at the previous round after clipping wrt to the standard deviation of tilde_sigma2k.
        :param k: integer (view index)
        """
        if self.DP_c == 'DIFF':
            const = self.DP_c_const
            if self.global_params is not None:
                diff_sigmak = deepcopy(self.Sigma2[k]-self.tilde_Sigma2k[k])
                C = sqrt(self.sigma_til_sigma2k[k])*const
                Clip = max(1,abs(diff_sigmak)/C)

                pert_sk = laplace.rvs(scale=2.0*C / self.DP_e[2][k])
                
                if diff_sigmak/Clip+pert_sk <= -1.*self.tilde_Sigma2k[k]:
                    self.Sigma2[k] = deepcopy(1e-5)
                else:
                    self.Sigma2[k] = deepcopy(diff_sigmak/Clip+pert_sk+self.tilde_Sigma2k[k])
            else:
                dp_sigma = log(self.real_Sigma2[k])
                C = abs(dp_sigma)/4.
                pert_sk = laplace.rvs(scale=2*C / self.DP_e[2][k])
                self.Sigma2[k] = deepcopy(exp(dp_sigma/(max(1,abs(dp_sigma)/C) + pert_sk)))
        elif self.DP_c =='MODEL':
            dp_sigma = log(self.real_Sigma2[k])
            C = abs(log(self.tilde_Sigma2k))/4.
            pert_sk = laplace.rvs(scale=2*C / self.DP_e[2][k])
            self.Sigma2[k] = deepcopy(exp(dp_sigma/(max(1,abs(dp_sigma)/C) + pert_sk)))

    def E_L_c_perturbed(self):
        """
        This function evaluate the expected log-likelihood with the perturbed parameters.
        :return float
        """
        N = self.dataset.shape[0]

        norm2, tn_muk = self.compute_access_vectors(N,self.muk,self.Xk)
        M, B = self.eval_MB(self.Wk, self.Sigma2)
        E_X, E_X_2, E_L_c1 = self.compute_moments_LL(N, self.Sigma2, norm2, tn_muk, self.Wk, M, B)

        return E_L_c1

    def initial_loc_params(self):
        """
        This function initializes Wk and Sigmak.
        :return list of np.arrays (d_k x q) Wk
        :return list of floats > 0 Sigma2
        """
        q = self.n_components
        D_i = self.dim_views
        K = self.n_views

        Wk = []
        Sigma2 = []
        for k in range(K):
            if self.ViewsX[k] == 1:
                if self.global_params is None:
                    W_k = random_init_Wk(d_k=D_i[k],q=q,min_wi=-2.,max_wi=2.)
                    s = random_init_Sk(s1=.1,s2=.5)
                else:
                    W_k = matrix_normal.rvs(mean=self.tilde_Wk[k].reshape(D_i[k], q),
                                                    rowcov=np.eye(D_i[k]),
                                                    colcov=self.sigma_til_Wk[k]*np.eye(q)).reshape(D_i[k], q)
                    s = float(invgamma.rvs(a=self.Alpha[k], scale=self.Beta[k]))

                if self.q_i[k] < q:
                    W_k[:, self.q_i[k]:q] = 0
                Wk.append(W_k)
                Sigma2.append(s)
            else:
                Wk.append('NaN')
                Sigma2.append('NaN')

        return Wk, Sigma2

    def eval_muk(self,N,Wk,Sigma2,Xk):
        """
        Computes local parameter muk.
        :param N: integer (number of subjects)
        :param Wk: list of matrices (d_k x q)
        :param Sigma2: list of float > 0
        :param Xk: list of view-specific datasets
        :return list of np.arrays
        """

        D_i = self.dim_views
        K = self.n_views

        muk = []

        for k in range(K):
            if self.ViewsX[k] == 1:
                if ((self.global_params is None) or (self.tilde_muk[k] is None)):
                    mean_k = Xk[k].mean(axis=0).values.reshape(self.dim_views[k], 1)
                    muk.append(mean_k)
                else:
                    mu_1 = np.zeros((D_i[k], 1))
                    for n in range(N):
                        mu_1 += Xk[k].iloc[n].values.reshape(D_i[k], 1)
                    term1 = compute_inv_term1_muck(Wk[k], N, Sigma2[k], self.sigma_til_muk[k])
                    Cc = compute_Cck(Wk[k], Sigma2[k])
                    term2 = mu_1 + (1 / self.sigma_til_muk[k]) * Cc.dot(self.tilde_muk[k].reshape(D_i[k], 1))
                    muk.append(term1.dot(term2))
            else:
                muk.append('NaN')

        return muk

    def eval_MB(self, Wk, Sigma2):
        """
        Computes matrices M:=inv(I_q+sum_k Wk.TWk/sigma2k) and B:= [W1.T/sigma2K,...,W1.T/sigma2K].
        :param Wk: list of matrices (d_k x q)
        :param Sigma2: list of float > 0
        :return np.arrays
        """

        q = self.n_components
        D_i = self.dim_views
        K = self.n_views

        M1 = Wk[self.index].reshape(D_i[self.index], q).T.dot(Wk[self.index].reshape(D_i[self.index],q)) / Sigma2[self.index]
        B = Wk[self.index].reshape(D_i[self.index], q).T / Sigma2[self.index]
        for k in range(self.index + 1, K):
            if self.ViewsX[k] == 1:
                M1 += Wk[k].reshape(D_i[k], q).T.dot(Wk[k].reshape(D_i[k],q)) / Sigma2[k]
                B = np.concatenate((B, (Wk[k].reshape(D_i[k], q)).T / Sigma2[k]), axis=1)

        M = solve(np.eye(q) + M1,np.eye(q))

        return M, B

    def compute_access_vectors(self,N,muk,Xk):
        """
        Computes list of accessory vectors needed for further evaluations: 
        norm**2 of (tn^kg-mu^k-W^kag), (tn^kg-mu^k-W^kag), (tn^kg-mu^k).
        :param N: integer (number of subjects)
        :param muk: list of vectors (d_k x 1)
        :param Xk: list of view-specific datasets
        :return list of floats norm2
        :return list of np.arrays
        """

        D_i = self.dim_views
        K = self.n_views

        norm2 = [] # norm**2 of (tn^kg-mu^k)
        tn_muk = [] # (tn^kg-mu^k)
        for n in range(N):
            norm2_k = []
            tn_mu_k = []
            for k in range(K):
                if self.ViewsX[k] == 1:
                    tn_mu_k.append(Xk[k].iloc[n].values.reshape(D_i[k], 1) - muk[k])
                    norm2_k.append(np.linalg.norm(tn_mu_k[k]) ** 2)
                else:
                    norm2_k.append('NaN')
                    tn_mu_k.append('NaN')
            norm2.append(norm2_k)
            tn_muk.append(tn_mu_k)

        return norm2, tn_muk

    def compute_moments_LL(self, N, Sigma2, norm2, tn_muk, Wk, M, B):
        """
        Computes mean and second moment for each x_n, then expected log-likelihood.
        :param N: integer (number of subjects)
        :param Sigma2: list of floats > 0
        :param norm2: list of floats: norm**2 of (tn^kg-mu^k)
        :param tn_muk: list of vectors (d_k x 1)
        :param Wk: list of matrices (d_k x q)
        :param M: matrix (q x q)
        :param B: matrix (q x d_k)
        :return list of np.arrays E_X
        :return list of np.arrays E_X_2
        :return float E_L_c
        """

        q = self.n_components
        D_i = self.dim_views
        K = self.n_views

        E_X = []
        E_X_2 = []
        E_L_c = 0.0

        for n in range(N):
            tn_mu = tn_muk[n][self.index]
            for k in range(self.index+1,K):
                if self.ViewsX[k] == 1:
                    tn_mu = np.concatenate((tn_mu, tn_muk[n][k]), axis=0)
            E_X.append(M.dot(B).dot(tn_mu))

            E_X_2.append(M + E_X[n].dot(E_X[n].T))

            E_L_c_k = 0.0
            for k in range(K):
                if self.ViewsX[k] == 1:
                    E_L_c_k += - D_i[k] * log(Sigma2[k])/ 2.0 - (norm2[n][k] / 2 + np.matrix.trace(
                        (Wk[k].reshape(D_i[k], q)).T.dot(Wk[k].reshape(D_i[k], q)) * E_X_2[n]) / 2 - E_X[n].T.dot(
                        (Wk[k].reshape(D_i[k], q)).T).dot(tn_muk[n][k])) / Sigma2[k]
            E_L_c += float(E_L_c_k  - np.matrix.trace(E_X_2[n]) / 2.0)

        return E_X, E_X_2, E_L_c

    def eval_Wk_Sigma2_new(self, N, norm2, tn_muk, E_X, E_X_2, Sigma2):
        """
        Computes local parameter Wk and Sigma2k.
        :param N: integer (number of subjects)
        :param norm2: list of floats: norm**2 of (tn^kg-mu^k)
        :param tn_muk: list of vectors (d_k x 1): (tn^kg-mu^k)
        :param E_X: list of vectors (q x 1): mean of x_n
        :param E_X_2: list of matrices (q x q): second moment of x_n
        :param Sigma2: list of floats > 0
        :return list of np.arrays Wk
        :return list of floats > 0
        """

        q = self.n_components
        D_i = self.dim_views
        K = self.n_views

        Wk = []
        Sigma2_new = []

        for k in range(K):
            if self.ViewsX[k] == 1:
                W_1_1 = (tn_muk[0][k]).dot(E_X[0].T)
                W_2_2 = sum(E_X_2)
                for n in range(1,N):
                    W_1_1 += (tn_muk[n][k]).dot(E_X[n].T)

                if ((self.global_params is None) or (self.tilde_Wk[k] is None)):
                    W_1 = W_1_1

                    W_2 = solve(W_2_2, np.eye(q))
                else:
                    W_1 = W_1_1 + (Sigma2[k] / self.sigma_til_Wk[k]) * self.tilde_Wk[k]

                    W_2 = solve(W_2_2 + (Sigma2[k] / self.sigma_til_Wk[k]) * np.eye(q), np.eye(q))

                W_k = W_1.dot(W_2)
                if self.q_i[k] < q:
                    W_k[:, self.q_i[k]:q] = 0
                Wk.append(W_k)

                sigma2k = 0.0
                for n in range(N):
                    sigma2k += float(norm2[n][k] + np.matrix.trace(
                        (Wk[k].reshape(D_i[k], q)).T.dot(Wk[k].reshape(D_i[k], q)) * E_X_2[n]) - 2 * E_X[n].T.dot(
                        (Wk[k].reshape(D_i[k], q)).T).dot(tn_muk[n][k]))
                if ((self.global_params is None) or (self.tilde_Sigma2k[k] is None)):
                    var = 1  # variance of the Inverse-Gamma prior
                    alpha = 1.0 / (4 * var) + 2
                    beta = (alpha - 1) / 2
                    sigma2k_N = (sigma2k + 2 * beta) / (N * D_i[k] + 2 * (alpha + 1))  ## prior=inverse gamma
                    while sigma2k_N <= 0:  # while til obtention of a non negative sigma2k: each round the variance of the Inverse Gamma is divided by 2
                        var *= 1.0 / 2
                        alpha = 1.0 / (4 * var) + 2
                        beta = (alpha - 1) / 2
                        sigma2k_N = (sigma2k + 2 * beta) / (N * D_i[k] + 2 * (alpha + 1))  ## prior=inverse gamma
                    if var != 1:
                        print(f'Variance of Inverse-Gamma for sigma2(%i) = {var}' % (k + 1))
                    Sigma2_new.append(sigma2k_N)
                else:
                    Sigma2_new.append((sigma2k + 2 * self.Beta[k]) / (N * D_i[k] + 2 * (self.Alpha[k] + 1)))
            else:
                Wk.append('NaN')
                Sigma2_new.append('NaN')

        return Wk, Sigma2_new

    def append_results_local(self, E_L_c, Wk, muk, Sigma2):
        """
        Append all results for record.
        :param E_L_c: float (expected log-likelihood)
        :param Wk: list of matrices (d_k x q)
        :param muk: list of vectors (d_k x 1)
        :param Sigma2: list of floats > 0
        :return pandas series
        """

        K = self.n_views

        results_iter = pd.Series(name=self.ep_count, dtype=float)
        results_iter['iter'] = self.ep_count
        results_iter['E_L_c'] = E_L_c
        for k in range(K):
            name1 = 'W_norm_' + str(k + 1)
            name2 = 'mu_norm_' + str(k + 1)
            name3 = 'sigma2_' + str(k + 1)
            if self.ViewsX[k] == 1:
                results_iter[name1] = np.linalg.norm(Wk[k])
                results_iter[name2] = np.linalg.norm(muk[k])
                results_iter[name3] = Sigma2[k]
            else:
                results_iter[name1] = 0
                results_iter[name2] = 0
                results_iter[name3] = 0
        results_iter['center'] = self.id_client

        return results_iter
