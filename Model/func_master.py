import numpy as np
from math import exp
from scipy.special import digamma, polygamma

def inv_digamma(y, eps=1e-8, max_iter=100):
    """
    Computes Numerical inverse to the digamma function by root finding.
    :return float
    """
    '''Numerical inverse to the digamma function by root finding'''

    if y >= -2.22:
        xold = np.exp(y) + 0.5
    else:
        xold = -1 / (y - digamma(1))

    for _ in range(max_iter):

        xnew = xold - (digamma(xold) - y) / polygamma(1, xold)

        if np.abs(xold - xnew) < eps:
            break

        xold = xnew

    return xnew