import sys
import pandas as pd
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
from math import sqrt, ceil
import numpy as np
import os
from os.path import dirname
from sklearn import preprocessing
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics import confusion_matrix
from csv import writer
from datetime import datetime
from scipy.stats import norm
import argparse
sys.path.append(dirname(dirname(__file__)))

from utils import t_n_k, Diffs, append_list_as_row, csv_for_records, df_sim_tnk, Initial_priors
from Model.client import Fed_MVPPCA_CLIENT
from Model.master import Fed_MVPPCA_MASTER
from Data.Partition_data import split_Dataset

matplotlib.use('agg')
sns.set()

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Fed_MVPPCA')
    parser.add_argument('--data', metavar='-d', type=str, default='SD', choices = ['SD', 'ADNI'],
                        help='Dataset')
    parser.add_argument('--scenario', metavar='-s', type=str, default='IID', choices = ['IID', 'G', 'K', 'GK'],
                        help='Scenario for data splitting')
    parser.add_argument('--centers', metavar='-c', type=int, default=4,
                        help='Number of centers')
    parser.add_argument('--rounds', metavar='-r', type=int, default=30,
                        help='Number of rounds')
    parser.add_argument('--MAP_iter', metavar='-MAP_i', type=int, default=15,
                        help='Number of iterations for MAP (EM with prior)')
    parser.add_argument('--EM_iter', metavar='-EM_i', type=int, default=30,
                        help='Number of iterations for EM (first local optimization round)')
    parser.add_argument('--latent_dim', metavar='-ld', type=int, default=2,
                        help='Dimension latent space')
    parser.add_argument('--id', metavar='-i', type=str, default='loc',
                        help='Test id')
    parser.add_argument('--norm', metavar='-n', type=int, default=1, choices = [0, 1],
                        help='normalize data. 0 = False; 1 = True')
    parser.add_argument('--N_WAIC', metavar='-w', type=int, default=100,
                        help='Number of parameters simulations for WAIC')
    parser.add_argument('--diff_privacy', metavar='-DP', type=int, default=0, choices = [0, 1],
                        help='send differentially private parameters. 0 = False; 1 = True')
    parser.add_argument('--DP_epsilon', metavar='-DP_e',  type=float, default=10.,
                        help='Epsilon for differential privacy for each parameter')
    parser.add_argument('--DP_delta', metavar='-DP_d', type=float, default=0.01,
                        help='Delta for differential privacy for each parameter')
    parser.add_argument('--DP_clip', metavar='-DP_c', type=str, default='DIFF', choices = ['DIFF', 'MODEL'],
                        help='Type of clipping. DIFF=difference clipping, MODEL=model clipping')
    parser.add_argument('--DP_c_const', metavar='-DP_c_c', type=float, default=1.,
                        help='Constant for clipping')

    args = parser.parse_args()

    # csv file for records
    script_dir = os.path.dirname(__file__)

    # initialization

    N_Centers = args.centers # number of centers
    N_rounds = args.rounds # number of rounds (centers-master)
    CENTERS = range(N_Centers)

    n_comp = args.latent_dim  # number of components
    if args.scenario == 'IID':
        rho = 1e-5  # this parameter is used exclusively if only one center is contributing to the estimation of one view/group parameters
    else:
        rho = 1e-2

    assert args.diff_privacy in [0, 1]
    # Define dictionary for differential privacy
    DP = {'DP': bool(args.diff_privacy)}

    if DP['DP']:
        results_csv, Params_csv, std_csv, LS_csv = csv_for_records(script_dir, args.scenario, DP['DP'])
    else:
        results_csv, Params_csv, std_csv = csv_for_records(script_dir, args.scenario, DP['DP'])

    print('==Loading and splitting data (3CV)==')

    if args.scenario != 'IID':
        assert N_Centers in [3, 6], 'For heterogeneous settings, the number of centers should be either 3 or 6'
    data = split_Dataset(dataset=args.data, N_Centers=N_Centers, Scenario=args.scenario)

    groups = data['char']['groups']
    D_i = data['char']['views']
    G = len(groups)  # total number of groups
    K = len(D_i)  # total number of views

    if DP['DP']:
        DP['DP_e'] = [[args.DP_epsilon for _ in range(K)] for _ in range(3)]
        DP['DP_d'] = [[args.DP_delta for _ in range(K)] for _ in range(2)]
        DP['DP_c'] = args.DP_clip
        DP['DP_c_const'] = args.DP_c_const
    else:
        DP['DP_c'] = None

    del data['char']

    print('Folds', len(data.keys()), args.scenario)

    for fold in data.keys():

        # Id characterizing current test
        Test_id = datetime.now().strftime('%Y-%m-%d %H:%M:%S') + '_' + args.id  # run id for records

        # Create datasets for saving results
        results = pd.DataFrame()  # results of local estimations
        Global_res = pd.DataFrame()  # results of global estimations

        # folder for saving figs
        name_res_dir = 'Fed_MVPPCA_Results_' + args.scenario + '_' + Test_id + '/'
        results_dir = os.path.join(script_dir, name_res_dir)

        if not os.path.isdir(results_dir):
            os.makedirs(results_dir)

        XC = data[fold]['train_sets'][0]
        Y_train = data[fold]['train_sets'][1]
        ViewsXC = data[fold]['train_sets'][2]
        Test_data = data[fold]['test_set'][0]
        Y_test = data[fold]['test_set'][1]
        ViewsTest = data[fold]['test_set'][2]

        if args.scenario in ('K','GK'):
            X_absent_view = data[fold]['test_set'][3]
            # save absent views to csv for plots
            X_absent_view[1].to_csv(results_dir + "C2_V2.csv")
            X_absent_view[2].to_csv(results_dir + "C3_V3.csv")

        CENTERS = range(N_Centers)
        N_subjects = [xc.shape[0] for xc in XC]
        N_MAP_iter = [args.MAP_iter for c in CENTERS]
        N_EM_iter = [args.EM_iter for c in CENTERS]

        # Save first columns of t_n_k to csv for plots
        Tk = t_n_k(XC, ViewsXC, D_i, CENTERS, K, bool(args.norm))
        Tk.to_csv(results_dir + "Tnk.csv")

        ROUNDS = range(1, N_rounds + 1)
        # if DP['DP']:
        #     G_params = Initial_priors(K,n_comp,D_i)
        # else:
        G_params = None 
        ep_count = [0 for _ in CENTERS] # Save number of epochs completed for each center

        # ====================================== #
        #  ITERATION CENTERS-MASTER STARTS HERE  #
        # ====================================== #
        print('===Start federated training===')
        r = 1
        while r <= N_rounds:
            print(f'==Round {r}==')
            L_params_new = [[], [], []]
            if DP['DP']:
                epsilon_fin = []
                delta_fin = []
                LL_or = []
                LL_1 = []
            # Each center optimize its local parameters on the local dataset using priors provided by the master
            for c in CENTERS:
                if r == 1:
                    DP_r = {'DP': False}
                    n_iterations = N_EM_iter[c]
                else:
                    DP_r = DP
                    n_iterations = N_MAP_iter[c]
                    # if ((r > 10) & (r <= 30)):
                    #     DP['DP_e'] = [[5.0 for _ in range(K)] for _ in range(3)]
                    # elif r > 30:
                    #     DP['DP_e'] = [[1.0 for _ in range(K)] for _ in range(3)]
                local_model = Fed_MVPPCA_CLIENT(XC[c], ViewsX=ViewsXC[c], norm=bool(args.norm), global_params=G_params,
                                                dim_views=D_i, n_components=n_comp,
                                                n_iterations=n_iterations, id_client=str(c+1), 
                                                round=r, ep_count=ep_count[c], DP = DP_r)
                results = results.append(local_model.fit(), ignore_index=True)
                muk, Wk, Sigma2 = local_model.local_params
                tot_ep_c = local_model.tot_epochs
                # Append parameters updates to be communicated to the master
                L_params_new[0].append(muk)
                L_params_new[1].append(Wk)
                L_params_new[2].append(Sigma2)
                ep_count[c]=tot_ep_c
                if DP_r['DP']:
                    e_d = local_model.eps_del
                    epsilon_fin.append(e_d[0])
                    delta_fin.append(e_d[1])
                    LL = local_model.LL
                    LL_or.append(LL[0])
                    LL_1.append(LL[1])
            # The master updates global parameters after having collected all local updates
            results['center'] = pd.Categorical(results['center'], ordered=True,
                                               categories=[str(c + 1) for c in CENTERS])
            global_model = Fed_MVPPCA_MASTER(local_params=L_params_new, n_centers=N_Centers, 
                                             dim_views=D_i, n_components=n_comp,
                                             rho=rho, round=r)
            # Save global parameters updates to be communicated to the centers
            Global_res = Global_res.append(global_model.fit(), ignore_index=True)
            tilde_muk, tilde_Wk, tilde_Sigma2k, Alpha, Beta, sigma_til_muk, sigma_til_Wk, sigma_til_sigma2k = global_model.global_params
            G_params = [tilde_muk, tilde_Wk, tilde_Sigma2k, Alpha, Beta, sigma_til_muk, sigma_til_Wk, sigma_til_sigma2k]

            if DP_r['DP']:
                row_contents = [Test_id, args.data, N_Centers, r, ViewsXC, N_MAP_iter, N_EM_iter, 
                                args.DP_clip, epsilon_fin, delta_fin, LL_or, LL_1]
                append_list_as_row(LS_csv, row_contents)
            row_contents = [Test_id, args.data, N_Centers, r, ViewsXC, N_MAP_iter, N_EM_iter,
                            DP['DP'], DP['DP_c'], sigma_til_muk, sigma_til_Wk, sigma_til_sigma2k]
            append_list_as_row(std_csv, row_contents)

            # the federated optimization can not end if any global parameter is missing
            if r == N_rounds:
                if (any(elem is None for elem in tilde_muk) or any(elem is None for elem in tilde_Wk) or any(elem is None for elem in tilde_Sigma2k)):
                    N_rounds += 1
            r += 1
        # ====================================== #
        # ====================================== #

        # save local results to csv
        results.to_csv(results_dir + "Centers_results.csv")

        # save global results to csv
        Global_res.to_csv(results_dir + "Master_results.csv")

        # append estimated parameters to csv
        # List of strings
        row_contents = [Test_id, args.data, DP['DP'], DP['DP_c'], G_params[5], G_params[6], G_params[3], G_params[4]]
        # Append row_contents as new line to Model_comparaison.csv
        append_list_as_row(Params_csv, row_contents)

        print('==Generating and saving results==')
        Tksim, Eigenval_cov, Max_eigen = df_sim_tnk(global_model,K,D_i)

        # save simulations of t_n(k) to csv
        Tksim.to_csv(results_dir + "Tnksim.csv")

        Data = pd.DataFrame()
        tot_simu = []
        WAIC_score = []
        MAE = []
        Latent_Train = pd.DataFrame()
        Label_Train = pd.Series()
        for c in CENTERS:
            local_model = Fed_MVPPCA_CLIENT(XC[c], ViewsX=ViewsXC[c], norm=bool(args.norm), global_params=G_params,
                                            dim_views=D_i, n_components=n_comp, n_iterations=0,
                                            id_client=str(c+1), round=N_rounds + 1, DP = {'DP': False})
            # Dataframe of latent space for LDA
            Latent_Train = Latent_Train.append(local_model.simu_latent())
            Label_Train = Label_Train.append(Y_train[c])
            # Mean Absolute Error
            MAE.append(local_model.MAE())
            # WAIC
            WAIC, newS = local_model.WAIC_score(args.N_WAIC)
            WAIC_score.append(WAIC)
            tot_simu.append(newS)
            # simulation of x_n|t_n using global parameters
            X_simu_g = local_model.simuXn().reshape(n_comp, 1)
            for s in range(499):
                Xsimg = local_model.simuXn().reshape(n_comp, 1)
                X_simu_g = np.concatenate((X_simu_g, Xsimg), axis=1)
            len_X_simu_g = len(X_simu_g[0, :])
            Xn_giv_tn = pd.DataFrame({'X_n_0': list(X_simu_g[0, :]), 'X_n_1': list(X_simu_g[1, :]),
                                    'Center': [str(c + 1) for _ in range(len_X_simu_g)],
                                    'Round': [(r) for _ in range(len_X_simu_g)]})
            Data = Data.append(Xn_giv_tn, ignore_index=True)
        Data['Center'] = pd.Categorical(Data['Center'], ordered=True,
                                               categories=[str(c + 1) for c in CENTERS])

        # save simulations of x_n|t_n
        Data.to_csv(results_dir + "Xn_given_tn.csv")

        lda = LinearDiscriminantAnalysis()
        X_Train_lda = lda.fit_transform(Latent_Train, Label_Train.reindex(Latent_Train.index))

        # Evaluate global accuracy for test dataset
        MAE_test = []
        Latent_Test = pd.DataFrame()
        Label_Test = pd.Series()
        for c_test in range(len(Test_data)):
            local_model = Fed_MVPPCA_CLIENT(Test_data[c_test], ViewsX=ViewsTest[c_test], norm=args.norm,
                                            global_params=G_params, dim_views=D_i,
                                            n_components=n_comp, n_iterations=0, id_client=str(N_Centers + 1 + c_test), round=1, DP = {'DP': False})
            # Dataframe of latent space for LDA
            Latent_Test = Latent_Test.append(local_model.simu_latent())
            Label_Test = Label_Test.append(Y_test[c_test])
            # MAE Test
            MAE_test.append(local_model.MAE())
        # LDA Test
        Size_tes = Latent_Test.shape[0]
        y_pred_test = lda.predict(Latent_Test)
        conf_LDA_Test = confusion_matrix(Label_Test.reindex(Latent_Test.index), y_pred_test)
        TP = np.diag(conf_LDA_Test)
        num_classes = len(np.unique(Label_Test))
        accuracy_LDA_Test = sum(TP) / Size_tes

        # Difference among estimated local and global parameters
        Diff_1,Diff_2,Diff_3 = Diffs(G_params, L_params_new, CENTERS, ViewsXC, K)

        # Append results to csv for model comparaison
        # List of strings
        row_contents = [Test_id, args.data, DP['DP'], DP['DP_c'], N_rounds, N_MAP_iter, 
                        N_EM_iter, D_i, N_Centers, N_subjects, n_comp,
                        tot_simu, WAIC_score, Diff_1, Diff_2, Diff_3, 
                        Eigenval_cov, Max_eigen, MAE, MAE_test,
                        accuracy_LDA_Test, conf_LDA_Test]
        # Append row_contents as new line to Model_comparaison.csv
        append_list_as_row(results_csv, row_contents)

        # Print and save true vs prediction for excluded views in test dataset, and save for plotting
        if args.scenario in ('K', 'GK'):
            Pred_csv_name = 'Prediction_k_' + args.scenario + '_' + '.csv'
            pred_k_csv = os.path.join(script_dir, Pred_csv_name)
            if not os.path.isfile(pred_k_csv):
                header = ['Test_id', 'Center_id', 'View_id', 'meank', 'Diag_V']
                with open(pred_k_csv, "w", newline='') as f:
                    writer(f, delimiter=',').writerow(header)  # write the header

            tilde_muk = G_params[0]
            tilde_Wk = G_params[1]
            tilde_Sigma2k = G_params[2]
            for c in range(len(Test_data)):
                if sum(ViewsTest[c]) < K:
                    if bool(args.norm):
                        x = X_absent_view[c].values
                        min_max_scaler = preprocessing.MinMaxScaler()
                        x_scaled = min_max_scaler.fit_transform(x)
                        X_absent_view[c] = pd.DataFrame(x_scaled, index=X_absent_view[c].index,
                                                        columns=X_absent_view[c].columns)
                    N_sbj = X_absent_view[c].shape[0]
                    for k in range(K):
                        if ViewsXC[c][k] == 0:
                            plt.figure(figsize=(5 * 4, 5 * ceil(D_i[k] / 4)), dpi=150)
                            meank = tilde_muk[k]
                            V = tilde_Wk[k].dot(tilde_Wk[k].T) + tilde_Sigma2k[k] * np.eye(D_i[k])
                            Diag_V = V.diagonal()
                            row_contents = [Test_id, c, k, meank, Diag_V]
                            append_list_as_row(pred_k_csv, row_contents)
                            ind = 1
                            for d in range(D_i[k]):
                                plt.subplot(ceil(D_i[k] / 4), 4, ind).set_title('$(t^{(%i)})_{%i}$' % ((k + 1), d))
                                mean = meank[d][0]
                                std = sqrt(Diag_V[d])
                                plt.hist(X_absent_view[c].iloc[:, d].tolist(), density=True, alpha=0.6,
                                         color='grey', label='data')
                                xmin = min(X_absent_view[c].iloc[:, d].tolist()) - 0.2
                                xmax = max(X_absent_view[c].iloc[:, d].tolist()) + 0.2
                                x = np.linspace(xmin, xmax, 100)
                                y = norm.pdf(x, mean, std)
                                plt.plot(x, y, linewidth=3, color='blue', label='prediction, view = %i' % (k + 1))
                                ind += 1
                            plt.legend()
                            plt.savefig(results_dir + "Missing_view_pred_k=%i_c=%i" % ((k + 1), (c + 1)))
                            plt.clf()
                            plt.close()
