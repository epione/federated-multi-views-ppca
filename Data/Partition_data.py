import os
import numpy as np
import pandas as pd
from sklearn.model_selection import StratifiedKFold, train_test_split
from copy import deepcopy
import sys
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
data_dir = os.path.dirname(__file__)


ADNI_CSV = os.path.join(data_dir, 'data_irene_dx_2g_corrected.csv')
SYNT_CSV = os.path.join(data_dir, 'Synthetic_data_2g.csv')


def load_csv(dataset='SD'):
    if dataset=='SD':
        X = pd.read_csv(SYNT_CSV, index_col=0)
    elif dataset=='ADNI':
        X = pd.read_csv(ADNI_CSV, index_col=0)

    # Remove spaces and lowercase feature names
    X.columns = [col.strip().lower() for col in list(X.columns)]
    X.rename(columns={'dx': 'label'}, inplace=True)
    return X

def split_Dataset(dataset='SD', N_Centers=3, Scenario='IID'):
    X = load_csv(dataset=dataset)
    if dataset=='SD':
        D_i = [15, 8, 10]
    elif dataset == 'ADNI':
        D_i = [7, 41, 41, 41]
    groups = np.unique(X['label'])  # total groups in dataset

    data = {}
    data['char'] = {'groups': groups, 'views': D_i}

    # stratify +
    skf = StratifiedKFold(n_splits=3, random_state=21, shuffle=True)
    for fold_i, (train_index, test_index) in enumerate(skf.split(X.drop(columns='label'), X.label), 1):
        X_train, X_test = X.drop(columns='label').iloc[train_index, :], X.drop(columns='label').iloc[test_index, :]
        y_train, y_test = X.label.iloc[train_index], X.label.iloc[test_index]
        Test_data_init = X_test

        # >= 1 train center
        if N_Centers == 1:
            XC = [X_train]
        else:
            yX_train = pd.concat([y_train, X_train], axis=1)
            yX_train.columns.values[0] = 'label'
            yX_train.columns = [col.strip() for col in list(X.columns)]

            # Define number of subjects per group in each center
            N_subj_gr = [[] for _ in range(N_Centers)]  # Number of subjects per group in each center
            if 'G' in Scenario:
                NC3 = N_Centers//3
                for n_C in range(N_Centers):
                    if n_C < NC3:
                        for gr in range(len(groups)):
                            N_subj_gr[n_C].append((yX_train.loc[yX_train.label == groups[gr], :].shape[0]) // (N_Centers-NC3))
                    elif ((n_C>=NC3) and (n_C<2*NC3)):
                        N_subj_gr[n_C].append(0)
                        for gr in range(1,len(groups)):
                            N_subj_gr[n_C].append((yX_train.loc[yX_train.label == groups[gr], :].shape[0]) // (N_Centers-NC3))
                    elif n_C>=2*NC3:
                        for gr in range(len(groups)-1):
                            N_subj_gr[n_C].append((yX_train.loc[yX_train.label == groups[gr], :].shape[0]) // (N_Centers-NC3))
                        N_subj_gr[n_C].append(0)
            else:
                # if data are splitted IID wrt groups:
                for n_C in range(N_Centers):
                    for gr in range(len(groups)):
                        N_subj_gr[n_C].append((yX_train.loc[yX_train.label == groups[gr], :].shape[0]) // N_Centers)
            for gr in range(len(groups)):
                rem_gr = (yX_train.loc[yX_train.label == groups[gr], :].shape[0])-\
                    sum([N_subj_gr[i][gr] for i in range(N_Centers)])
                N_subj_gr[0][gr] += rem_gr

            # split data in centers
            XC = []
            index_gr = []
            X_C_gr = yX_train.loc[yX_train.label == groups[0], :].sample(n=N_subj_gr[0][0], random_state=21)
            index_gr.append(X_C_gr.index)
            for gr in range(1, len(groups)):
                X_C_gr_g = yX_train.loc[yX_train.label == groups[gr], :].sample(n=N_subj_gr[0][gr], random_state=21)
                index_gr.append(X_C_gr_g.index)
                X_C_gr = pd.concat([X_C_gr, X_C_gr_g], axis=0)
            XC.append(X_C_gr)
            # XC.append(X_C_gr.drop(columns='label'))
            for c in range(1, N_Centers):
                X_C_gr = yX_train.loc[yX_train.label == groups[0], :].drop(index_gr[0], axis=0).sample(n=N_subj_gr[c][0],
                                                                                                       random_state=21)
                index_gr[0] = index_gr[0].union(X_C_gr.index)
                for gr in range(1, len(groups)):
                    X_C_gr_g = yX_train.loc[yX_train.label == groups[gr], :].drop(index_gr[gr], axis=0).sample(
                        n=N_subj_gr[c][gr], random_state=21)
                    index_gr[gr] = index_gr[gr].union(X_C_gr_g.index)
                    X_C_gr = pd.concat([X_C_gr, X_C_gr_g], axis=0)
                XC.append(X_C_gr)
                # XC.append(X_C_gr.drop(columns='label'))

        # if scenario include missing views: delete views in some center
        if 'K' in Scenario:
            NC3 = N_Centers//3
            ViewsXC = [[1 for _ in range(len(D_i))] for _ in range(NC3)]
            for n_C in range(N_Centers):
                if ((n_C>=NC3) and (n_C<2*NC3)):
                    XC[n_C] = deepcopy(XC[n_C].drop(XC[n_C].columns[list(range(D_i[0] + 1, D_i[0] + D_i[1] + 1))], axis=1))
                    viewsc = [1 for _ in range(len(D_i))]
                    viewsc[1] = 0
                    ViewsXC.append(viewsc)
                elif n_C>=2*NC3:
                    XC[n_C] = deepcopy(XC[n_C].drop(XC[n_C].columns[list(range(D_i[0] + D_i[1] + 1, D_i[0] + D_i[1] + D_i[2] + 1))],
                                                axis=1))
                    viewsc = [1 for _ in range(len(D_i))]
                    viewsc[2] = 0
                    ViewsXC.append(viewsc)
        else:
            ViewsXC = [[1 for _ in range(len(D_i))] for _ in range(N_Centers)]

        # Get labels from data
        YC = [X.loc[df.index, 'label'] for df in XC]

        # Drop labels from XC[c]
        if N_Centers > 1:
            for n_C in range(N_Centers):
                XC[n_C] = XC[n_C].drop(columns='label') 

        if 'K' in Scenario:
            yX_test = pd.concat([y_test, X_test], axis=1)
            # Define number of subjects per group in each test set
            N_subj_gr = [[] for _ in range(3)] 
            if 'G' in Scenario:
                for gr in range(len(groups)):
                    N_subj_gr[0].append((yX_test.loc[yX_test.label == groups[gr], :].shape[0]) // 2)
                N_subj_gr[1].append(0)
                for gr in range(1,len(groups)):
                    N_subj_gr[1].append((yX_test.loc[yX_test.label == groups[gr], :].shape[0]) // 2)
                for gr in range(len(groups)-1):
                    N_subj_gr[2].append((yX_test.loc[yX_test.label == groups[gr], :].shape[0]) // 2)
                N_subj_gr[2].append(0)
            else:
                for n_t in range(3):
                    for gr in range(len(groups)):
                        N_subj_gr[n_t].append((yX_test.loc[yX_test.label == groups[gr], :].shape[0]) // N_Centers)
            for gr in range(len(groups)):
                rem_gr = (yX_test.loc[yX_test.label == groups[gr], :].shape[0])-\
                    sum([N_subj_gr[i][gr] for i in range(3)])
                N_subj_gr[0][gr] += rem_gr

            # split data in testsets
            XC_test = []
            index_gr = []
            test = yX_test.loc[yX_test.label == groups[0], :].sample(n=N_subj_gr[0][0], random_state=21)
            index_gr.append(test.index)
            for gr in range(1, len(groups)):
                test_g = yX_test.loc[yX_test.label == groups[gr], :].sample(n=N_subj_gr[0][gr], random_state=21)
                index_gr.append(test_g.index)
                test = pd.concat([test, test_g], axis=0)
            XC_test.append(test)
            for c in range(1, 3):
                test = yX_test.loc[yX_test.label == groups[0], :].drop(index_gr[0], axis=0).sample(n=N_subj_gr[c][0],
                                                                                                       random_state=21)
                index_gr[0] = index_gr[0].union(test.index)
                for gr in range(1, len(groups)):
                    test_g = yX_test.loc[yX_test.label == groups[gr], :].drop(index_gr[gr], axis=0).sample(
                        n=N_subj_gr[c][gr], random_state=21)
                    index_gr[gr] = index_gr[gr].union(test_g.index)
                    test = pd.concat([test, test_g], axis=0)
                XC_test.append(test)
            
            y_test_fin = [XC_test[0]['label'],XC_test[1]['label'],XC_test[2]['label']]
            ViewsTest = [[1 for _ in range(len(D_i))] for _ in range(3)]
            ViewsTest[1][1] = 0
            ViewsTest[2][2] = 0
            # save absent views to csv for plots
            X_absent_view = [0]

            V2_test = deepcopy(XC_test[1].drop(columns='label'))
            V2_abs = pd.concat([V2_test.iloc[:, 0], V2_test.iloc[:, D_i[0]:D_i[0] + D_i[1]]], axis=1)
            X_absent_view.append(V2_abs)
            V2_test = deepcopy(V2_test.drop(V2_test.columns[list(range(D_i[0], D_i[0] + D_i[1]))], axis=1))

            V3_test = deepcopy(XC_test[2].drop(columns='label'))
            V3_abs = pd.concat([V3_test.iloc[:, 0], V3_test.iloc[:, D_i[0] + D_i[1]:D_i[0] + D_i[1] + D_i[2]]], axis=1)
            X_absent_view.append(V3_abs)
            V3_test = deepcopy(V3_test.drop(V3_test.columns[(range(D_i[0] + D_i[1], D_i[0] + D_i[1] + D_i[2]))], axis=1))

            Test_data = [XC_test[0].drop(columns='label'), V2_test, V3_test]

            test_set = (Test_data, y_test_fin, ViewsTest, X_absent_view)
        else:
            y_test = X.loc[Test_data_init.index, 'label']
            ViewsTest = [1 for _ in range(len(D_i))]
            test_set = ([Test_data_init], [y_test], [ViewsTest])

        data[fold_i] = {'train_sets': (XC, YC, ViewsXC), 'test_set': test_set}
    return data
